﻿using Blackbaud.AppFx.WebAPI.ServiceProxy;
using Blackbaud.AppFx.XmlTypes.DataForms;
using System;
using System.Net;
using System.Configuration;
using Blackbaud.AppFx.XmlTypes;


namespace WebAPIDemo.Helpers
{
    using Helpers;
    internal class GlobalChangeHelper
    {
        // Returns information on a given global change definition
        // Returns information on a given global change definition
        public static void GlobalChangeInformation(string GlobalChangeCatalogID)
        {
            //GlobalChangeInformationRequest request = new GlobalChangeInformationRequest();

            // Provide the appropriateID from the BUSINESSPROCESSCATALOG  table for the 'Global Change Process' business process
            // Description: Returns information on a given global change definition
            //Guid systemRecordID = new Guid(GlobalChangeCatalogID);
            //request.CatalogID = systemRecordID;
            //request.ClientAppInfo = new ClientAppInfoHeader();
            //request.ClientAppInfo.ClientAppName = "VCU Test";

            // Create request
            GlobalChangeInformationRequest request = new GlobalChangeInformationRequest
            {
                CatalogID = new Guid(GlobalChangeCatalogID),
                ClientAppInfo = new ClientAppInfoHeader
                {
                    ClientAppName = "Sample Application",
                    REDatabaseToUse = ConfigurationManager.AppSettings["RADARdatabase"]
                }
            };


            //Call the operation passing the request and catching the reply
            try
            {
                GlobalChangeInformationReply reply = ApiHelper.ApiService().GlobalChangeInformation(request);



                FormField[] formfieldvalues = reply.FormMetadata.FormFields;

                Console.WriteLine("Displaying GlobalChangeInformation defined within the global change definition (GlobalChangeSpec)...");
                Console.WriteLine("Name: {0} | Value: {1}", "CatalogID", reply.CatalogID.ToString());
                Console.WriteLine("Name: {0} | Value: {1}", "DisplayName", reply.DisplayName.ToString());

                Console.WriteLine("Displaying Some GlobalChangeInformation Form Field Metadata defined within the global change definition...");
                for (int i = 0; i < formfieldvalues.Length; i++)
                {
                    Console.WriteLine("FieldID: {0} | Caption: {1} | Required: {2} | DataType: {3}", formfieldvalues[i].FieldID.ToString(), formfieldvalues[i].Caption.ToString(), formfieldvalues[i].Required.ToString(), formfieldvalues[i].DataType.ToString());
                }

                Console.WriteLine("Displaying GlobalChangeInformation completed");
                Console.WriteLine("");
                Console.WriteLine("");
            }
            catch (Exception ex)
            {
                Console.WriteLine("SoapException caught: {0} ", ex.Message.ToString());
            }
        }



    }
}
