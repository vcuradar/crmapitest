﻿using Blackbaud.AppFx.WebAPI.ServiceProxy;
using Blackbaud.AppFx.XmlTypes.DataForms;
using System;
using System.Net;
using System.Configuration;
using Blackbaud.AppFx.XmlTypes;


namespace WebAPIDemo.Helpers
{
    using Helpers;
    internal class ConstituentHelper
    {
        private static void StartNameSearch(string input)
        {
            string firstName = string.Empty;
            string lastName = string.Empty;
            string lookupId = string.Empty;

            // Process input
            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine("No input was received.");
            }
            else
            {
                string[] words = input.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                firstName = words[0].Trim();

                if (firstName.StartsWith("8-"))
                {
                    Console.WriteLine(IDSearch(lastName));
                }
                else if (firstName.ToLower() == "new:")
                {
                    NewConstituent(words[2].Trim(), words[1].Trim());
                }
                else
                {

                    if (words.Length > 1)
                    {
                        lastName = words[1].Trim();
                    }
                    Console.WriteLine(NameSearch(lastName, firstName));
                }
            }
        }

        private static string NameSearch(string lastName, string firstName)
        {
            string results = "";

            // Define filters for the search list
            DataFormFieldValueSet fieldValueSet = new DataFormFieldValueSet()
            {
                new DataFormFieldValue("KEYNAME", lastName),
                new DataFormFieldValue("FIRSTNAME", firstName),
                new DataFormFieldValue("INCLUDEORGANIZATIONS", false),
                new DataFormFieldValue("INCLUDEGROUPS", false)
            };

            DataFormItem filter = new DataFormItem
            {
                Values = fieldValueSet
            };

            var request = ApiConstituentByNameOrLookupIdSearch();
            request.Filter = filter;

            // Run search
            SearchListLoadReply reply = ApiHelper.ApiService().SearchListLoad(request);

            return ShowResults(reply.Output.Rows);
        }

        private static string IDSearch(string lookupId)
        {
            string results = "";

            // Define filters for the search list
            DataFormFieldValueSet fieldValueSet = new DataFormFieldValueSet()
            {
                new DataFormFieldValue("LOOKUPID", lookupId),
            };

            DataFormItem filter = new DataFormItem
            {
                Values = fieldValueSet
            };

            var request = ApiConstituentByNameOrLookupIdSearch();
            request.Filter = filter;

            // Run search
            SearchListLoadReply reply = ApiHelper.ApiService().SearchListLoad(request);

            return ShowResults(reply.Output.Rows);
        }


        public static SearchListLoadRequest ApiConstituentByNameOrLookupIdSearch()
        {
            // Create request
            SearchListLoadRequest request = new SearchListLoadRequest
            {
                SearchListID = new Guid("fdf9d631-5277-4300-80b3-fdf5fb8850ec"), // Constituent Search by Name or Lookup ID
                ClientAppInfo = new ClientAppInfoHeader
                {
                    ClientAppName = "Sample Application",
                    REDatabaseToUse = ConfigurationManager.AppSettings["RADARdatabase"]
                }
            };

            return request;
        }


        // Search List: Constituent Search by Name or Lookup ID:
        // https://bbisrig03stg.blackbaudhosting.com/59760Stg_a7b0c803-16f4-41bf-b7e9-8d35ef414400/webui/webshellpage.aspx?SITEINFOID=a7b0c803-16f4-41bf-b7e9-8d35ef414400&HOSTEDSITEINFOID=a7b0c803-16f4-41bf-b7e9-8d35ef414400&databasename=59760Stg#pageType=p&pageId=36455595-2213-48df-9a19-11796b046c88&tabId=ec9e4f8d-ef2c-4752-a09b-8948c6f2c76a&recordId=fdf9d631-5277-4300-80b3-fdf5fb8850ec

        // Field that it returns on are 'Output' tab
        // Field to be filtered/parameters on 'Fields' tab

        public static string ShowResults(ListOutputRow[] reply)
        {
            if (reply != null)
            {
                string results = "Lookup ID\tName\tClass";
                foreach (ListOutputRow row in reply)
                {
                    results += row.Values[1].ToString() + "\t" + row.Values[2].ToString() + "\t" + row.Values[10].ToString() + "\t" + Environment.NewLine;
                }

                return results;
            }
            else
            {
                return "There were no search results.";
            }
        }



        public static string NewConstituent(string keyname, string firstname)
        {
            string results = "";

            // Define filters for the search list
            DataFormFieldValueSet fieldValueSet = new DataFormFieldValueSet()
            {
                new DataFormFieldValue("LASTNAME", keyname),
                new DataFormFieldValue("GENDERCODE", 0),
                new DataFormFieldValue("FIRSTNAME", firstname)
            };

            DataFormItem filter = new DataFormItem
            {
                Values = fieldValueSet
            };

            var request = ApiConstituentSaveDataForm();
            request.DataFormItem = filter;

            DataFormSaveReply reply = ApiHelper.ApiService().DataFormSave(request);

            return "done";//ShowResults(reply.Output.Rows);
        }

        public static DataFormSaveRequest ApiConstituentSaveDataForm()
        {
            // Create request
            DataFormSaveRequest request = new DataFormSaveRequest
            {
                FormID = new Guid("1986f0cf-efb6-48b3-afde-950b57562434"), // Individual Add Form
                ClientAppInfo = new ClientAppInfoHeader
                {
                    ClientAppName = "Sample Application",
                    REDatabaseToUse = ConfigurationManager.AppSettings["RADARdatabase"]
                }
            };

            return request;
        }


    }
}
