﻿using Blackbaud.AppFx.WebAPI.ServiceProxy;
using Blackbaud.AppFx.XmlTypes.DataForms;
using System;
using System.Net;
using System.Configuration;
using Blackbaud.AppFx.XmlTypes;


namespace WebAPIDemo.Helpers
{
    using Helpers;
    internal class DataListHelper
    {
        public static string GetDataListErrors()
        {
            var request = MakeRequest("b96f1310-b2c2-44d8-a23e-8fe4f1ae8b51");// Failed ETL DTSX packages
            // request.Filter = filter;

            // Run search
            DataListLoadReply reply = ApiHelper.ApiService().DataListLoad(request);

            return ShowDataListResults(reply);
        }

        public static string ShowDataListResults(DataListLoadReply reply)
        {
            if (reply != null)
            {
                string results = reply.TotalRowsInReply + " Rows\nDate\tETL";

                foreach (DataListResultRow row in reply.Rows)
                {
                    results += row.Values[1].ToString() + "\t" + row.Values[2].ToString() + "\t" + Environment.NewLine;
                }

                return results;
            }
            else
            {
                return "There were no search results.";
            }
        }

        public static DataListLoadRequest MakeRequest(string id)
        {
            // Create request
            DataListLoadRequest request = new DataListLoadRequest
            {
                DataListID = new Guid(id),
                ClientAppInfo = new ClientAppInfoHeader
                {
                    ClientAppName = "Sample Application",
                    REDatabaseToUse = ConfigurationManager.AppSettings["RADARdatabase"]
                }
            };

            return request;
        }
    }
}
