﻿using Blackbaud.AppFx.WebAPI.ServiceProxy;
using Blackbaud.AppFx.XmlTypes.DataForms;
using System;
using System.Net;
using System.Configuration;
using Blackbaud.AppFx.XmlTypes;


namespace WebAPIDemo.Helpers
{
    using Helpers;
    internal class ApiHelper
    {
        // Returns information on a given global change definition
        public static AppFxWebService ApiService()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            // Initialize web service
            AppFxWebService service = new AppFxWebService
            {
                Url = "https://bbisrig03stg.blackbaudhosting.com/59760Stg_a7b0c803-16f4-41bf-b7e9-8d35ef414400/AppFxWebService.asmx"
            };

            // Set credentials
            NetworkCredential credentials = new NetworkCredential
            {
                Domain = ConfigurationManager.AppSettings["RADARdomain"],
                UserName = ConfigurationManager.AppSettings["RADARuser"],
                Password = ConfigurationManager.AppSettings["RADARpassword"]
            };
            service.Credentials = credentials;


            return service;
        }
    }
}
