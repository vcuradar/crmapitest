﻿using Blackbaud.AppFx.WebAPI.ServiceProxy;
using Blackbaud.AppFx.XmlTypes.DataForms;
using System;
using System.Net;
using System.Configuration;
using Blackbaud.AppFx.XmlTypes;
using System.Management.Automation;
using System.Management.Automation.Runspaces;

namespace WebAPIDemo.Helpers
{
    using Helpers;
    using System.Collections.ObjectModel;
    using System.IO;

    internal class PowerShellHelper
    {
        // Returns information on a given global change definition
        public static void RunFile()
        {
            string FilePath = @"\\bridgetrak\c$\test\";

            var script = File.ReadAllText(FilePath + "testps.ps1");
            var scriptFile = FilePath + "testps.ps1";

        // ERROR:
        //    Powershell caught: Access to the registry key 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\PowerShell\1\ShellIds\Microsoft.PowerShell' is denied.
        //       To change the execution policy for the default(LocalMachine) scope, start Windows PowerShell with the "Run as administrator" option.
        //       To change the execution policy for the current user, run "Set-ExecutionPolicy -Scope CurrentUser".


            //try
            //{
            //    PowerShell ps = PowerShell.Create();
            //    ps.AddScript(@"C:\Users\rmdowns\Documents\GitHub\eCRM-WebAPI-Sample\WebAPIDemo\testps.ps1", true).Invoke();
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine("Powershell caught: {0} ", ex.Message.ToString());
            //}

                    using (var runspace = RunspaceFactory.CreateRunspace())
            {
                try
                {
                    runspace.Open();
                    var ps = PowerShell.Create();
                    ps.Runspace = runspace;
                    ps.AddCommand("Set-ExecutionPolicy").AddArgument("Unrestricted");
                    ps.AddScript(script);
                    ps.Invoke();
                    var er = ps.HadErrors;
                    var su = ps.InvocationStateInfo.State;
                    foreach (var result in ps.Invoke())
                    {
                        var test = result;
                        var test2 = test;
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Powershell caught: {0} ", ex.Message.ToString());
                }
            }

            RunspaceConfiguration runspaceConfiguration = RunspaceConfiguration.Create();

            //using (Runspace runspace = RunspaceFactory.CreateRunspace(runspaceConfiguration))
            //{
            //    runspace.Open();
            //    RunspaceInvoke scriptInvoker = new RunspaceInvoke(runspace);
            //    scriptInvoker.Invoke("Set-ExecutionPolicy Unrestricted");
            //    Pipeline pipeline = runspace.CreatePipeline();
            //    Command scriptCommand = new Command(scriptFile);
            //    //Collection<CommandParameter> commandParameters = new Collection<CommandParameter>();
            //    //foreach (string scriptParameter in parameters)
            //    //{
            //    //    CommandParameter commandParm = new CommandParameter(null, scriptParameter);
            //    //    commandParameters.Add(commandParm);
            //    //    scriptCommand.Parameters.Add(commandParm);
            //    //}
            //    pipeline.Commands.Add(scriptCommand);
            //    Collection<PSObject> psObjects;
            //    psObjects = pipeline.Invoke();
            //}

        }
    }
}
