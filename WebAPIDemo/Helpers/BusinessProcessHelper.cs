﻿using Blackbaud.AppFx.WebAPI.ServiceProxy;
using Blackbaud.AppFx.XmlTypes.DataForms;
using System;
using System.Net;
using System.Configuration;
using Blackbaud.AppFx.XmlTypes;


namespace WebAPIDemo.Helpers
{
    using Helpers;
    internal class BusinessProcessHelper
    {
        // Returns information on a given global change definition
        public static void BusinessProcessInformation(string CatalogID)
        {
            //GlobalChangeInformationRequest request = new GlobalChangeInformationRequest();

            // Create request
            BusinessProcessInformationRequest request = new BusinessProcessInformationRequest
            {
                BusinessProcessID = new Guid(CatalogID),
                ClientAppInfo = new ClientAppInfoHeader
                {
                    ClientAppName = "Sample Application",
                    REDatabaseToUse = ConfigurationManager.AppSettings["RADARdatabase"]
                }
            };


            //Call the operation passing the request and catching the reply
            try
            {
                BusinessProcessInformationReply reply = ApiHelper.ApiService().BusinessProcessInformation(request);


                Console.WriteLine("Displaying BusinessProcessInformation defined within the business process definition (GlobalChangeSpec)...");
                Console.WriteLine("Name: {0} | Value: {1}", "CatalogID", reply.BusinessProcessID.ToString());
                Console.WriteLine("Name: {0} | Value: {1}", "DisplayName", reply.BusinessProcessName.ToString());

                Console.WriteLine("Displaying BusinessProcessInformation completed");
                Console.WriteLine("");
                Console.WriteLine("");
            }
            catch (Exception ex)
            {
                Console.WriteLine("SoapException caught: {0} ", ex.Message.ToString());
            }
        }

        // Shows what is running here:
        // https://bbisrig03stg.blackbaudhosting.com/59760Stg_a7b0c803-16f4-41bf-b7e9-8d35ef414400/webui/webshellpage.aspx?SITEINFOID=a7b0c803-16f4-41bf-b7e9-8d35ef414400&HOSTEDSITEINFOID=a7b0c803-16f4-41bf-b7e9-8d35ef414400&databasename=59760Stg#pageType=p&pageId=09140005-ad50-4259-b45c-376f7d0d8cbe&tabId=fea9b280-b520-4e55-be60-df80eac6daa2
        public static void RunBusinessProcess(string CatalogID, string ParamID)
        {

            // Create request
            BusinessProcessLaunchRequest request = new BusinessProcessLaunchRequest
            {
                BusinessProcessID = new Guid(CatalogID),
                ParameterSetID = new Guid(ParamID),
                ClientAppInfo = new ClientAppInfoHeader
                {
                    ClientAppName = "Sample Application",
                    REDatabaseToUse = ConfigurationManager.AppSettings["RADARdatabase"]
                }
            };


            //Call the operation passing the request and catching the reply
            try
            {
                BusinessProcessLaunchReply reply = ApiHelper.ApiService().BusinessProcessLaunch(request);


                Console.WriteLine("Displaying BusinessProcessLaunch reply:");
                Console.WriteLine("Name: {0} | Value: {1}", "BusinessProcessStatusID", reply.BusinessProcessStatusID.ToString());
                Console.WriteLine("Name: {0} | Value: {1}", "BusinessProcessName", reply.BusinessProcessName.ToString());
                Console.WriteLine("Name: {0} | Value: {1}", "NumberSuccessfullyProcessed", reply.NumberSuccessfullyProcessed.ToString());
                Console.WriteLine("Name: {0} | Value: {1}", "OutputData", reply.OutputData.ToString());
               // reply.OutputData

                Console.WriteLine("Displaying BusinessProcessInformation completed");
                Console.WriteLine("");
                Console.WriteLine("");
            }
            catch (Exception ex)
            {
                Console.WriteLine("SoapException caught: {0} ", ex.Message.ToString());
            }
        }
    }
}
