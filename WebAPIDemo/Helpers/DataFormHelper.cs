﻿using Blackbaud.AppFx.WebAPI.ServiceProxy;
using Blackbaud.AppFx.XmlTypes.DataForms;
using System;
using System.Net;
using System.Configuration;
using Blackbaud.AppFx.XmlTypes;


namespace WebAPIDemo.Helpers
{
    using Helpers;
    internal class DataFormHelper
    {
        public static string GetBPStatusDataForm(string id)
        {
            var request = MakeRequest(id, "7cf7a0c1-5848-40b2-b3d6-88a1406efc8b"); //business process view status data form);
            // request.Filter = filter;

            // Run search
            DataFormLoadReply reply = ApiHelper.ApiService().DataFormLoad(request);

            return ShowDataListResults(reply);
        }

        public static int GetBPStatus(string id)
        {
            var request = MakeRequest(id, "7cf7a0c1-5848-40b2-b3d6-88a1406efc8b"); //business process view status data form);
            // request.Filter = filter;

            // Run search
            DataFormLoadReply reply = ApiHelper.ApiService().DataFormLoad(request);

            if (reply != null)
            {
                string result = "Unknown";
                string result2 = "Unknown";

                foreach (DataFormFieldValue row in reply.DataFormItem.Values)
                {
                    if (row.ID == "STATUS")
                        result = row.Value.ToString();
                    if (row.ID == "COMPLETED")
                        result2 = row.Value.ToString();
                }

                if (result == "Completed" && result2 == "True")
                    return 1;
                else
                    return 0;
            }
            else
            {
                return 0;
            }
        }

        public static int GetETLStatus()
        {
            var request = MakeRequest("75a36279-8531-4a5f-afd6-483fe5d26037", "14d985df-6e99-420c-a90a-ad6582dcda2c");
            // request.Filter = filter;

            // Run search
            DataFormLoadReply reply = ApiHelper.ApiService().DataFormLoad(request);

            if (reply != null)
            {
                int result = 0;

                foreach (DataFormFieldValue row in reply.DataFormItem.Values)
                {
                    if (row.ID == "LASTEXECUTIONSTATUS")
                        result = Convert.ToInt32(row.Value);
                }

                return result;
            }
            else
            {
                return 0;
            }
        }

        public static string GetETLStatusDataForm()
        {
            var request = MakeRequest("75a36279-8531-4a5f-afd6-483fe5d26037", "14d985df-6e99-420c-a90a-ad6582dcda2c");
            // request.Filter = filter;

            // Run search
            DataFormLoadReply reply = ApiHelper.ApiService().DataFormLoad(request);

            return ShowDataListResults(reply);
        }

        

        public static string ShowDataListResults(DataFormLoadReply reply)
        {
            if (reply != null)
            {
                var test = reply.DataFormItem.XmlSerializerForDataFormFieldValues;
                var test2 = reply.DataFormItem.Values;
                var test3 = "test";
            }
            else
            {
                return "There were no search results.";
            }

            if (reply != null)
            {
                string results =" Rows";

                foreach (DataFormFieldValue row in reply.DataFormItem.Values)
                {
                    results += row.ID + "\t" + row.Value + "\t" + Environment.NewLine;
                }

                return results;
            }
            else
            {
                return "There were no search results.";
            }
        }


        public static DataFormLoadRequest MakeRequest(string id, string DataFormTypeId)
        {
            // Create request
            DataFormLoadRequest request = new DataFormLoadRequest
            {
                FormID = new Guid(DataFormTypeId),
                RecordID = id,
                ClientAppInfo = new ClientAppInfoHeader
                {
                    ClientAppName = "Sample Application",
                    REDatabaseToUse = ConfigurationManager.AppSettings["RADARdatabase"]
                }
            };

            return request;
        }
    }
}
