﻿using Blackbaud.AppFx.WebAPI.ServiceProxy;
using Blackbaud.AppFx.XmlTypes.DataForms;
using System;
using System.Net;
using System.Configuration;
using Blackbaud.AppFx.XmlTypes;


namespace WebAPIDemo.Helpers
{
    using Helpers;
    internal class DesignationHelper
    {
        private static string DesignationSearch(string Name)
        {
            string results = "";

            // Define filters for the search list
            DataFormFieldValueSet fieldValueSet = new DataFormFieldValueSet()
            {
                new DataFormFieldValue("COMBINEDSEARCH", Name)
            };

            DataFormItem filter = new DataFormItem
            {
                Values = fieldValueSet
            };

            var request = ApiDesignationSearch();
            request.Filter = filter;


            SearchListLoadReply reply = new SearchListLoadReply();
            try
            {
                // Run search
                reply = ApiHelper.ApiService().SearchListLoad(request);


            }
            catch (Exception ex)
            {
                var test = ex;
            }

            return ShowDesignationResults(reply.Output.Rows);
        }

        private static SearchListLoadRequest ApiDesignationSearch()
        {
            // Create request
            SearchListLoadRequest request = new SearchListLoadRequest
            {
                SearchListID = new Guid("3187706b-b1ee-410f-b599-27aee1b26237"), // Designation Search
                ClientAppInfo = new ClientAppInfoHeader
                {
                    ClientAppName = "Sample Application",
                    REDatabaseToUse = ConfigurationManager.AppSettings["RADARdatabase"]
                }
            };

            return request;
        }


        private static string ShowDesignationResults(ListOutputRow[] reply)
        {
            if (reply != null)
            {
                string results = "ID\tName\tReport Code 1";
                foreach (ListOutputRow row in reply)
                {
                    results += row.Values[0].ToString() + "\t" + row.Values[1].ToString() + "\t" + row.Values[7].ToString() + "\t" + Environment.NewLine;
                }

                return results;
            }
            else
            {
                return "There were no search results.";
            }
        }

    }
}
