﻿using Blackbaud.AppFx.WebAPI.ServiceProxy;
using Blackbaud.AppFx.XmlTypes.DataForms;
using System;
using System.Net;
using System.Configuration;
using Blackbaud.AppFx.XmlTypes;


namespace WebAPIDemo.Helpers
{
    using Helpers;
    internal class TributeHelper
    {
        private static string TributeSearch(string Name)
        {
            string results = "";

            // Define filters for the search list
            DataFormFieldValueSet fieldValueSet = new DataFormFieldValueSet()
            {
                new DataFormFieldValue("TRIBUTETEXT", Name),
                new DataFormFieldValue("TRIBUTEEKEYNAME", Name),
                new DataFormFieldValue("TRIBUTEELOOKUP", Name),
                new DataFormFieldValue("ACKNOWLEDGEEKEYNAME", Name)
            };

            DataFormItem filter = new DataFormItem
            {
                Values = fieldValueSet
            };

            var request = ApiTributeSearch();
            request.Filter = filter;

            // Run search
            SearchListLoadReply reply = ApiHelper.ApiService().SearchListLoad(request);

            return ShowTributeResults(reply.Output.Rows);
        }

        private static SearchListLoadRequest ApiTributeSearch()
        {
            // Create request
            SearchListLoadRequest request = new SearchListLoadRequest
            {
                SearchListID = new Guid("7b8b5cb6-89ea-4135-87b8-1d9ae4dd6aad"), // Tribute Search Extend
                ClientAppInfo = new ClientAppInfoHeader
                {
                    ClientAppName = "Sample Application",
                    REDatabaseToUse = ConfigurationManager.AppSettings["RADARdatabase"]
                }
            };

            return request;
        }

        private static string ShowTributeResults(ListOutputRow[] reply)
        {
            if (reply != null)
            {
                string results = "ID\tName\tReport Code 1";
                foreach (ListOutputRow row in reply)
                {
                    results += row.Values[0].ToString() + "\t" + row.Values[1].ToString() + "\t" + row.Values[7].ToString() + "\t" + Environment.NewLine;
                }

                return results;
            }
            else
            {
                return "There were no search results.";
            }
        }
    }
}
