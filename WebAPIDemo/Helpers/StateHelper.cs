﻿using Blackbaud.AppFx.WebAPI.ServiceProxy;
using Blackbaud.AppFx.XmlTypes.DataForms;
using System;
using System.Net;
using System.Configuration;
using Blackbaud.AppFx.XmlTypes;


namespace WebAPIDemo.Helpers
{
    using Helpers;
    internal class StateHelper
    {

        private static string StateSearch(string abbr)
        {
            string results = "";

            // Define filters for the search list
            DataFormFieldValueSet fieldValueSet = new DataFormFieldValueSet()
            {
                new DataFormFieldValue("LOOKUPID", abbr),
            };

            DataFormItem filter = new DataFormItem
            {
                Values = fieldValueSet
            };

            var request = ConstituentHelper.ApiConstituentByNameOrLookupIdSearch();
            request.Filter = filter;

            // Run search
            SearchListLoadReply reply = ApiHelper.ApiService().SearchListLoad(request);

            return ShowStateResults(reply.Output.Rows);
        }



        private static SimpleDataListLoadRequest ApiStateSearch()
        {
            // Create request
            SimpleDataListLoadRequest request = new SimpleDataListLoadRequest
            {
                DataListID = new Guid("7fa91401-596c-4f7c-936d-6e41683121d7"), // State Abbreviation List
                ClientAppInfo = new ClientAppInfoHeader
                {
                    ClientAppName = "Sample Application",
                    REDatabaseToUse = ConfigurationManager.AppSettings["RADARdatabase"]
                }
            };

            return request;
        }


        private static string ShowStateResults(ListOutputRow[] reply)
        {
            if (reply != null)
            {
                string results = "Lookup ID\tName\tClass";
                foreach (ListOutputRow row in reply)
                {
                    results += row.Values[1].ToString() + "\t" + row.Values[2].ToString() + "\t" + row.Values[10].ToString() + "\t" + Environment.NewLine;
                }

                return results;
            }
            else
            {
                return "There were no search results.";
            }
        }


    }
}
