﻿using Blackbaud.AppFx.WebAPI.ServiceProxy;
using Blackbaud.AppFx.XmlTypes.DataForms;
using System;
using System.Net;
using System.Configuration;
using Blackbaud.AppFx.XmlTypes;


// Endpoint documentation can be found at the Endpoint URL:
// https://bbisrig03stg.blackbaudhosting.com/59760Stg_a7b0c803-16f4-41bf-b7e9-8d35ef414400/AppFxWebService.asmx

namespace WebAPIDemo
{
    using Helpers;
    internal class Program
    {
        static void Main(string[] args)
        {
            // if no args, then prompt
            if (args.Length == 0)
                ManualEntry();
            else
            {
                ProcessCommands(String.Join(" ", args));
            }
        }

        private static void ManualEntry()
        {
            Console.WriteLine("Enter your search critera using 'Firstname Lastname' or 'LookupID' -- to make a new records use 'new: firstname lastname':");
            var input = Console.ReadLine();
            ProcessCommands(input);
            ManualEntry();
        }

        private static void ProcessCommands(string input)
        {

            // Process input
            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine("No input was received.");

            }
            else
            {
                string[] words = input.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                // PowerShellHelper.RunFile();


                //Console.WriteLine(Environment.NewLine + "--------- etl process ---------");
                if(DataFormHelper.GetETLStatus() == 1)
                {
                    Console.WriteLine("ETL successful");
                }
                else
                {
                    Console.WriteLine(DataFormHelper.GetETLStatusDataForm()); // etl process
                }

                //Console.WriteLine(Environment.NewLine + "--------- data warehouse incremental process ---------");
                if (DataFormHelper.GetBPStatus("D1C1B806-6D9B-4146-B5B8-8CC59A075286") == 1)
                {
                    Console.WriteLine("data warehouse incremental process successful");
                }
                else
                {
                    Console.WriteLine(DataFormHelper.GetBPStatusDataForm("D1C1B806-6D9B-4146-B5B8-8CC59A075286")); // post etl process
                }

                //Console.WriteLine(Environment.NewLine + "--------- post etl process ---------");
                if (DataFormHelper.GetBPStatus("bceba28a-ca96-4c7c-8746-4976be33e31f") == 1)
                {
                    Console.WriteLine("post etl process successful");
                }
                else
                {
                    Console.WriteLine(DataFormHelper.GetBPStatusDataForm("bceba28a-ca96-4c7c-8746-4976be33e31f")); // post etl process
                }

                //Console.WriteLine(Environment.NewLine + "--------- sync process ---------");
                if (DataFormHelper.GetBPStatus("9dfd5924-a5e9-4ebd-baba-a241b5dc9a71") == 1)
                {
                    Console.WriteLine("bbdw sync process successful");
                }
                else
                {
                    Console.WriteLine(DataFormHelper.GetBPStatusDataForm("9dfd5924-a5e9-4ebd-baba-a241b5dc9a71")); // bbdw sync process
                }
               

                //Console.WriteLine(Environment.NewLine + "--------- test process ---------");
                if (DataFormHelper.GetBPStatus("36fd7ed0-a647-417d-af6b-aa645a81c7cb") == 1)
                {
                    Console.WriteLine("update email process successful");
                }
                else
                {
                    Console.WriteLine(DataFormHelper.GetBPStatusDataForm("36fd7ed0-a647-417d-af6b-aa645a81c7cb")); // Update Primary Email GC
                }

                

                // run client sync

                // check QA processes

                // check database status

                // run advizor process


                // BusinessProcessHelper.RunBusinessProcess("3269A1D1-31CB-4D28-945C-B7623A3EFCCA", "36fd7ed0-a647-417d-af6b-aa645a81c7cb"); // Global Change | Update Primary Email

                //095f4585-00bd-41ac-b841-2a9d4194fc6b = BBDW Sync Business Process Catalog ID
                //9DFD5924-A5E9-4EBD-BABA-A241B5DC9A71 = BBDW Sync Business Process Param ID

                // Check to make sure ETL ran

                // Check to make sure POST ETL ran

                // Check to make sure BBDW Sync ran



                //SELECT * FROM [BUSINESSPROCESSINSTANCE] where BUSINESSPROCESSPARAMETERSETID = '36fd7ed0-a647-417d-af6b-aa645a81c7cb'

                // BusinessProcessHelper.BusinessProcessInformation("DEFD98E8-B915-4C32-9ECF-05447EA5B5B1"); //[BUSINESSPROCESSCATALOG].ID  WORKS!

                //  BusinessProcessHelper.BusinessProcessInformation("DEFD98E8-B915-4C32-9ECF-05447EA5B5B1"); //[BUSINESSPROCESSCATALOG].ID  WORKS!


                //GlobalChangeInformation("BCEBA28A-CA96-4C7C-8746-4976BE33E31F"); // QUEUEPROCESS.ID

                // GlobalChangeInformation("2C6D6BBB-5264-408E-9E1B-4B99939C7C5F");
                // GlobalChangeInformation("06284915-0137-4A18-88A0-2129792972BC");
                // GlobalChangeHelper.GlobalChangeInformation("15469aee-a0e5-446f-a5d3-f23c7a8985a0"); // SettingAttribute primary employment


                //GlobalChangeHelper.GlobalChangeInformation("06284915-0137-4A18-88A0-2129792972BC"); //[GLOBALCHANGECATALOG].ID


                // Console.WriteLine(DesignationSearch(input));


            }
        }

        




 








    }
}
